package packageg30124.vilcica.adela.lab3.ex5;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Thing;
import becker.robots.Wall;

public class ex5 {
	public static void main(String[] args)
	   {
	      // Set up the initial situation
	      City ny = new City();
	      Thing parcel = new Thing(ny, 2, 2);
	      Wall blockAve0 = new Wall(ny, 1, 1, Direction.WEST);
	      Wall blockAve1 = new Wall(ny, 2, 1, Direction.WEST);
	      Wall blockAve2 = new Wall(ny, 2, 1, Direction.SOUTH);
	      Wall blockAve3 = new Wall(ny, 1, 2, Direction.SOUTH);
	      Wall blockAve5 = new Wall(ny, 1, 2, Direction.EAST);
	      Wall blockAve6 = new Wall(ny, 1, 2, Direction.NORTH);
	      Wall blockAve7 = new Wall(ny, 1, 1, Direction.NORTH);
	      Robot mark = new Robot(ny, 1, 2, Direction.SOUTH);
	      //turn to right with 3 moves
	      for(int i=0;i<=2;i++) {
	    	  mark.turnLeft();
	      }
	      
	      mark.move();
	      mark.turnLeft();
	      mark.move();
	      mark.turnLeft();
	      mark.move();
	      mark.pickThing();
	     
	      for(int i=0;i<=1;i++) {
		      mark.turnLeft();
		      }
		      
	      mark.move();
	     
	      for(int i=0;i<=2;i++) {
		      mark.turnLeft();
		      }
		      
	      mark.move();
	     
	      for(int i=0;i<=2;i++) {
		      mark.turnLeft();
		      }
		      
	      mark.move();
	      mark.putThing();
	     
}
}