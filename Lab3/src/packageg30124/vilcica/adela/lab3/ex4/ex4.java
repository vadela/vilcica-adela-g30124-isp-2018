package packageg30124.vilcica.adela.lab3.ex4;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Wall;

public class ex4
{
   public static void main(String[] args)
   {
      // Set up the initial situation
      City ny = new City();
      Wall blockAve0 = new Wall(ny, 1, 2, Direction.WEST);
      Wall blockAve1 = new Wall(ny, 2, 2, Direction.WEST);
      Wall blockAve2 = new Wall(ny, 2, 2, Direction.SOUTH);
      Wall blockAve3 = new Wall(ny, 2, 3, Direction.SOUTH);
      Wall blockAve4 = new Wall(ny, 2, 3, Direction.EAST);
      Wall blockAve5 = new Wall(ny, 1, 3, Direction.EAST);
      Wall blockAve6 = new Wall(ny, 1, 3, Direction.NORTH);
      Wall blockAve7 = new Wall(ny, 1, 2, Direction.NORTH);
      Robot mark = new Robot(ny, 0, 3, Direction.WEST);
      //robot move forward
      for(int i=0;i<=1;i++) {
      mark.move();
      }
      
      mark.turnLeft(); 
      
      for(int i=0;i<=2;i++) {
      mark.move();
      }
      
      mark.turnLeft(); 
      
      for(int i=0;i<=2;i++) {
          mark.move();
          }
     
      mark.turnLeft(); 

      for(int i=0;i<=2;i++) {
          mark.move();
          }
      
      mark.turnLeft(); 
      mark.move();
   }
} 