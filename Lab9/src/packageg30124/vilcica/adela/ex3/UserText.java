package packageg30124.vilcica.adela.ex3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class UserText extends JFrame {
	
	JLabel text;
	JTextField tText;
	JTextArea tArea;
	JButton bEnter;
	
	UserText(){
		setTitle("Ex 3 ISP");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		init();
		setSize(200,300);
		setVisible(true);
	}
	
	public void init() {
		this.setLayout(null);
		
		int width=80;
		int height=20;
		
		text = new JLabel("Text file: ");
		text.setBounds(10,50,width,height);
		
		tText = new JTextField();
		tText.setBounds(70,50,width,height);
		
		bEnter = new JButton("Enter");
		bEnter.setBounds(10,150,width,height);
		
		bEnter.addActionListener(new TratareButon());
		
		tArea = new JTextArea();
		tArea.setBounds(10,180,150,80);
		
		add(text);
		add(tText);
		add(bEnter);
		add(tArea);

	}
	class TratareButon implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub

	        String filename = UserText.this.tText.getText();
	        try {
	            BufferedReader in = new BufferedReader(new FileReader(filename));
	            String s;
	            while ((s = in.readLine()) != null) {
	            	UserText.this.tArea.append(s);
	            	UserText.this.tArea.append("\n");
	            }
	            in.close();
	        } catch (FileNotFoundException e1) {
	            e1.printStackTrace();
	            UserText.this.tArea.setText("Error 404, file not found");
	        } catch (IOException e1) {
	            e1.printStackTrace();
	        }
	    }
		 
	}
	public static void main(String args[])
    {
        new UserText();
    }
	
}
