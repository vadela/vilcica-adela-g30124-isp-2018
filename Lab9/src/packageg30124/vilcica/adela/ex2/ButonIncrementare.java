package packageg30124.vilcica.adela.ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButonIncrementare extends JFrame{

        JTextArea tArea;
        JButton buton;

        ButonIncrementare(){

            setTitle("Incrementare");
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            init();
            setSize(200,300);
            setVisible(true);
        }
        void init (){

            this.setLayout(null);
            int width=80;int height = 20;

            tArea=new JTextArea();
            tArea.setBounds(40, 40, width, height);

            buton=new JButton("Buton");
            buton.setBounds(40, 20, width, height);

            buton.addActionListener(new TratareButon());

            add(tArea);add(buton);
        }


        public static void main(String[] args) {
            ButonIncrementare b = new ButonIncrementare();
        }

        class TratareButon implements ActionListener {
            int i=0;

            //String tArea=ButonIncrementare.this.tArea.getText();

            public void actionPerformed(ActionEvent e){
                i++;
                ButonIncrementare.this.tArea.setText(" "+i);
            }
        }

    }