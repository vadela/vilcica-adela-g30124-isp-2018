package packageg30124.vilcica.adela.lab6.ex2;

import java.awt.Graphics;

public interface Shape {
	
    public abstract void draw(Graphics g);
    String getId();

	/*public void deleteById() {
	}/*/
}
