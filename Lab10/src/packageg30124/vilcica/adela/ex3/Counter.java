package packageg30124.vilcica.adela.ex3;


public class Counter extends Thread{
	String name;
	private Thread t;
	private int n1,n2;
	public Counter(String name, Thread t, int n1, int n2) {
		super();
		this.name = name;
		this.t = t;
		this.n1 = n1;
		this.n2 = n2;
	}
	@Override
	public void run()
    {
          System.out.println("Firul "+name+" a intrat in metoda run()");
          try
          {                
                if (t!=null) t.join();
                System.out.println("Firul "+name+" executa operatie.");
               
                for(int i=n1;i<n2;i++) {
                	System.out.println(i);
                	 Thread.sleep(50);
                }
                System.out.println("Firul "+name+" a terminat operatia.");
          }
          catch(Exception e){e.printStackTrace();} 
    }
	  public static void main(String[] args) {
          Counter c1 = new Counter("counter1",null,0,100);
          Counter c2 = new Counter("counter2",c1,100,200);
//          Counter c3 = new Counter("counter3");

//          c1.start();
//         c2.start();
//         c3.start();
//          
          c1.run();
          c2.run();
         
    }
	
}
