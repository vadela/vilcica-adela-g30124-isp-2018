package packageg30124.vilcica.adela.ex3;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import packageg30124.vilcica.adela.ex1.BankAccount;

public class Bank {
	private TreeSet<BankAccount>  banks = new TreeSet<>();
	
	public void addAccount(String owner, double balance) {
		BankAccount b = new BankAccount(owner,balance);
        banks.add(b);
	}
	
	public void printAccounts(){
		
		
		 Iterator<BankAccount> i = banks.iterator();
        while(i.hasNext()){
            BankAccount acc = i.next();
            System.out.println("Contul utilizatorului "+acc.getOwner()+" este "+acc.getBalance());
     
        }
		
	}	

	public void printAccounts(double minBalance, double maxBalance){
		
        Iterator<BankAccount> i = banks.iterator();
        while(i.hasNext()){
            BankAccount acc = i.next();
            if(acc.getBalance() > minBalance && acc.getBalance() < maxBalance)
            System.out.println(acc);
            System.out.println("Contul utilizatorului "+acc.getOwner()+" este "+acc.getBalance());
        }
	}

	public BankAccount getAccount(String owner){
    
	Iterator<BankAccount> i = banks.iterator();
    while(i.hasNext()){
        BankAccount acc = i.next();
        if(acc.getOwner().equalsIgnoreCase(owner))
            return acc;
    }
    return null;
}

public TreeSet<BankAccount> getAllAccounts() {

	return banks;
}

}
