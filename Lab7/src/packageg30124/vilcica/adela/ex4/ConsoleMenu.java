package packageg30124.vilcica.adela.ex4;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;

public class ConsoleMenu {
	
	public static void main(String[] args)throws Exception {

		HashMap<Word, Definition>hashMap=new HashMap<>();
		Dictionary dictionary=new Dictionary();
		
		char raspuns;
		
		String linie,explic;
		BufferedReader fluxIn=new BufferedReader(new InputStreamReader(System.in));
		
		do {
			
			System.out.println("Meniu:");
			System.out.println("a- Adauga cuvant");
			System.out.println("d- Arata definitia");
			System.out.println("w- Toate cuvintele");
			System.out.println("q- Toate definitiile");
			System.out.println("e- Iesire");
			
			linie=fluxIn.readLine();
			raspuns=linie.charAt(0);
			
			switch (raspuns) {
			case 'a':case 'A':
				System.out.println("Scrieti cuvantul:");
				linie=fluxIn.readLine();
				if(linie.length()>1) {
					System.out.println("Scrieti definitia:");
					explic=fluxIn.readLine();
					dictionary.addWord(new Word(linie), new Definition(explic));
				}
				break;
			
			case 'd':case 'D':
				System.out.println("Cautati cuvantul:");
				linie=fluxIn.readLine();
				if(linie.length()>1)
				{
					Definition explic1 = dictionary.getDefinition(new Word(linie));
					
					if (explic1 ==null) 
						System.out.println("Cuvantul nu afost gasit!");
					else
						System.out.println("Definitia:"+explic1.getDefinition());
				}
				break;
			
			case 'w':case'W':
			{
				dictionary.getAllWords();
				break;
			}
			
			case 'q':case'Q':
			{
				dictionary.getAllDefinitions();
				break;
			}
			}
			
		}while(raspuns!='e' && raspuns!='E');
		System.out.println("Program terminat.");
		
		
	}

}
