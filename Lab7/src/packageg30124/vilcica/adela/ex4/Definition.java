package packageg30124.vilcica.adela.ex4;

public class Definition {
	private String definition;

	public Definition(String definition) {
		this.definition=definition;
	}
	public void setDefinition(String definition) {
		this.definition = definition;
	}
	public String getDefinition() {
		return definition;
	}

}
