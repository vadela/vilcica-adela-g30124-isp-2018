package packageg30124.vilcica.adela.ex2;

import java.util.Collections;

import packageg30124.vilcica.adela.ex1.BankAccount;

public class Main {
	public static void main(String[] args) {
	
		Bank bank= new Bank();
		bank.addAccount("Adela", 200);
		bank.addAccount("bbbb", 100);
		bank.printAccounts();
		System.out.println("----");
		bank.printAccounts(120,1000);
		
		Collections.sort(bank.getAllAccounts(),BankAccount.bankcompare);
		bank.printAccounts2();
}
}