package packageg30124.vilcica.adela.ex2;
import packageg30124.vilcica.adela.ex1.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class Bank {
	ArrayList<BankAccount> bank =new ArrayList<>();

	public  void addAccount(String owner, double balance) {
		 BankAccount b = new BankAccount(owner,balance);
	        bank.add(b);
	}
	public void printAccounts() {
		Collections.sort(bank);
		  Iterator<BankAccount> i = bank.iterator();
        while(i.hasNext()){
	            BankAccount account = i.next();
	                System.out.println("Contul utilizatorului "+account.getOwner()+" este "+account.getBalance());
	}
}
	public void printAccounts(double minBalance, double maxBalance) {
	
		  Iterator<BankAccount> i = bank.iterator();
	        while(i.hasNext()){
	            BankAccount account = i.next();
				if(account.getBalance()>minBalance && account.getBalance()<maxBalance) {
			          System.out.println("Contul utilizatorului "+account.getOwner()+" este "+account.getBalance());
				}
	      }
}
	public ArrayList<BankAccount> getAllAccounts() {
		return bank;
	}
	public void printAccounts2() {
		
		  Iterator<BankAccount> i = bank.iterator();
        while(i.hasNext()){
	            BankAccount account = i.next();
	                System.out.println("Contul utilizatorului "+account.getOwner()+" este "+account.getBalance());
	}
}
}