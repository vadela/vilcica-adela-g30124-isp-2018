package packageg30124.vilcica.adela.ex4;


public class Main {
	
	
		public static void main(String[] args) throws Exception{
			CarManager cm=new CarManager();
			Car car1=cm.createCar("Skoda", 24245);
			Car car2=cm.createCar("Ford", 1233);
			Car car3 = cm.createCar("Fiat", 4211);
					
					
			cm.addCar(car1, "car1.txt");
			cm.addCar(car2, "car2.txt");
			cm.addCar(car3, "car3.txt");
			cm.removeCar("car1.txt");
			System.out.println(car3.toString());
			

		}
	}

