package packageg30124.vilcica.adela.ex4;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class CarManager {
	
	Car createCar(String model,int price) {
		Car car=new Car(model, price);
		return car;
	}
	void addCar(Car car,String store) throws IOException
	{
		ObjectOutputStream oStream=new ObjectOutputStream(new FileOutputStream(store));
		oStream.writeObject(car);
		System.out.println("A fost scris in fisier");
	}
	Car removeCar(String store) throws IOException,ClassNotFoundException
	{
		ObjectInputStream iStream=new ObjectInputStream(new FileInputStream(store));
		Car car=(Car)iStream.readObject();
		System.out.println("A fost citit din fisier urmatorul model: ");
		return car;
	}
	
}
