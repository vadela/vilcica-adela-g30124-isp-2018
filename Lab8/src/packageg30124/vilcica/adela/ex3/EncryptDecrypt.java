package packageg30124.vilcica.adela.ex3;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class EncryptDecrypt {
	
	public void enc(String st)throws IOException{
		
		BufferedReader br = new BufferedReader(new FileReader(st));
		String s = new String();
		String s1 = new String();
		
		while((s=br.readLine())!=null) {
			s1+=s+"\n";
		}
		br.close();
	
	
	DataOutputStream out = null;
	
	try {
		out= new DataOutputStream(new FileOutputStream("exemplu.enc"));
		
	
		for(char ch:s1.toCharArray()) {
			ch++;
			
			out.writeChar(ch);
		}
		out.close();
	}catch(FileNotFoundException e) {
		System.out.println("File not found"+e);
	}
}	
	
	public void dec(String st)throws IOException{
		BufferedReader br = new BufferedReader(new FileReader(st));
		String s = new String();
		String s1 = new String();
		
		while((s=br.readLine())!=null) {
			s1+=s+"\n";
		}
		br.close();
	
	
	DataOutputStream out = null;
	
	try {
		out= new DataOutputStream(new FileOutputStream("exemplu.dec"));
		
	
		for(char ch:s1.toCharArray()) {
			ch--;
			out.writeChar(ch);
		}
		out.close();
	}catch(FileNotFoundException e) {
		System.out.println("File not found"+e);
	}
		
	}

public static void main(String[] args) throws IOException {
	EncryptDecrypt ed = new EncryptDecrypt();
	
	System.out.println("Numele fisierul :");
	Scanner sc = new Scanner(System.in);
	String st = sc.nextLine();
	int ans=0;
	
		
	System.out.println("False, fisierul se cripteaza");
	System.out.println("True, fisierul se decripteaza");
	System.out.println("Raspuns= ");
	
	Scanner scanner = new Scanner(System.in);
	boolean rasp = scanner.nextBoolean();
	scanner.close();
	sc.close();

	
	if(rasp==false){
	ed.enc(st);
	} else {
		ed.dec(st);
		}
}
}
