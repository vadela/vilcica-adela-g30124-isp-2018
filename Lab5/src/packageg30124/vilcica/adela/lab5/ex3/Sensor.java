package packageg30124.vilcica.adela.lab5.ex3;

abstract class Sensor {
	String location;
	abstract int readValue();
	public String getLocation() {
		return location;
	}
}
