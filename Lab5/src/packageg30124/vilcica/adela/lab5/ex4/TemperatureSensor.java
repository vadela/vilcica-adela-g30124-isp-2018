package packageg30124.vilcica.adela.lab5.ex4;

import java.util.Random;

public class TemperatureSensor extends Sensor {
	
	private static TemperatureSensor ts;
	
	private TemperatureSensor() {
		
	}
	
	public static TemperatureSensor getTemperatureSensor() {
		if(ts==null) {
			ts = new TemperatureSensor();
		}
		return ts;
	}
	
	@Override
	int readValue() {
		 Random random = new Random();
	     int x = random.nextInt(100);
		 return x;
				}
	}


