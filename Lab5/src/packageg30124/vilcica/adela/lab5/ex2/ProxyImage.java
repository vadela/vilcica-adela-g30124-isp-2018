package packageg30124.vilcica.adela.lab5.ex2;

public class ProxyImage implements Image{
	 
	   private RealImage realImage;
	   private String fileName;
	 
	   public ProxyImage(String fileName){
	      this.fileName = fileName;
	   }
	   @Override
	public void Rotate() {
		      System.out.println("Display rotated " + fileName);
		   }
	   @Override
	   public void display() {
	      if(realImage == null){
	         realImage = new RealImage(fileName);
	      }
	      realImage.display();
	   }
	}