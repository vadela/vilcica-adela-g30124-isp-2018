package packageg30124.vilcica.adela.lab5.ex1;

public class Circle extends Shape {
	private double radius;
	public Circle() {
		radius=1.0;
	}
	public Circle(double radius) {
		this.radius=radius;
	}
	public Circle(double radius,String color,boolean filled) {
		super(color,filled);
		this.radius=radius;
	}
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	@Override
	public double getArea() {
		return Math.PI*(this.radius*this.radius);
	}
	@Override
	public double getPerimeter() {
		return 2*Math.PI*this.radius;
	}

	@Override
	public String toString() {
		super.toString();
		return "A Circle with radius=" + getRadius() + ", which is a subclass of" + super.toString() ;
	}
	
}
