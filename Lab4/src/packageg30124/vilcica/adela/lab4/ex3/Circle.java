package packageg30124.vilcica.adela.lab4.ex3;

public class Circle {
	private double radius=1.0;
	String color="red";
	
	public Circle() {
	
	}
	public Circle(double radius) {
		this.radius=radius;
	}
	public double getRadius() {
		return radius;
	}
	public double getArea() {
		return Math.PI*(this.radius*this.radius);
	}

}
