package packageg30124.vilcica.adela.lab4.ex1;

public class Box {
    private int id;

    public Box(Conveyor target, int pos, int id){
        this.id = id;
        target.addPackage(this,pos);
    }

    public int getId() {
        return id;
    }

    @Override
	public String toString(){
        return ""+id;
    }
}
