package packageg30124.vilcica.adela.lab4.ex5;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import packageg30124.vilcica.adela.lab4.ex4.Author;

public class TestBook {

	@Test
	public void testToString() {
		Author a1 = new Author("a2","a2@yahoo.com",'f');
		Book b1 = new Book("b1",a1,100.02,34);
		assertEquals("Book b1 by author a2 ( f ) at email a2@yahoo.com",b1.toString());
	}
	
	@Test
	public void testGet() {
		Author a1 = new Author("a2","a2@yahoo.com",'f');
		Book b1 = new Book("b1",a1,100.02,34);
		assertEquals("b1",b1.getName());
	}
}
