package packageg30124.vilcica.adela.lab4.ex6;

import static org.junit.Assert.assertEquals;

import packageg30124.vilcica.adela.lab4.ex4.Author;


public class TestBook {
	

	public void testToString() {
		Author[] a = new Author[3];
		a[0]=new Author("a0","a0@yahoo.com",'m');
				
		a[1]=new Author("a1","a1@yahoo.com",'f');
		Book b = new Book("b1",a, 40,22);
	
		assertEquals("Books b1 by 2 authors",b.toString());
	}
		public void testPrint() {
			Author[] a = new Author[3];
			a[0]=new Author("a0","a0@yahoo.com",'m');
					
			a[1]=new Author("a1","a1@yahoo.com",'f');
			Book b = new Book("b1",a, 40,22);
			b.printAuthors();
			assertEquals("a0",a[0].getName());
			assertEquals("a1",a[1].getName());	
		}
	}
	