package packageg30124.vilcica.adela.lab4.ex6;

import java.util.Arrays;

import packageg30124.vilcica.adela.lab4.ex4.Author;

public class Book {
	String name;
	Author[] authors;
	double price;
	int qtyInStock=0;
	public Book(String name,Author[] authors,double price,int qtyInStock) {
		this.name=name;
		this.authors=authors;
		this.price=price;
		this.qtyInStock=qtyInStock;
	}
	public String getName() {
		return name;
	}
	public Author[] getAuthor() {
		return authors;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getQtyInStock() {
		return qtyInStock;
	}
	public void setQtyInStock(int qtyInStock) {
		this.qtyInStock = qtyInStock;
	}


	@Override
	public String toString() {
		return "Books " + name + " by " +authors.length+ " authors";
	}
	public void printAuthors() {
		System.out.println("Authors are:"+Arrays.toString(authors));
	}
}
