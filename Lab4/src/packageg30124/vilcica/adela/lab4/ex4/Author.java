package packageg30124.vilcica.adela.lab4.ex4;

public class Author {
	private String name;
	private String email;
	private char gender;
	public Author(String name, String email, char gender) {
		this.name=name;
		this.email=email;
		this.gender=gender;
	}
	public String getName() {
		return this.name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public char getGender() {
		return gender;
	}
	@Override
	public String toString() {
		return  getName() + " ( "+ getGender() +" )"+" at email " + getEmail() ;
	}

}
