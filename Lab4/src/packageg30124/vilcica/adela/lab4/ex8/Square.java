package packageg30124.vilcica.adela.lab4.ex8;

public class Square extends Rectangle {
	public Square() {
		
	}
	public Square(double side) {
		super(side,side);
	}
	public Square(double side, String color,boolean filled) {
		super(side,side,color,filled);
	}
	public double getSide() {
		return getWidth();
	}
	public void setSide(double side) {
		setWidth(side);
	}
	@Override
	public void setWidth(double side) {
		setWidth(side);
    }
	@Override
	public void setLength(double side) {
		setWidth(side);
	}
	@Override
	public String toString() {
		super.toString();
		return "A Square with side=" + getSide() + ",which is a subclass of" + super.toString() ;
	}
	
}
