package packageg30124.vilcica.adela.lab4.ex7;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestCylinder {
	
	@Test
	public void testGetHeight() {
		Cylinder c1 = new Cylinder();
		Cylinder c2 = new Cylinder(13.4);
		Cylinder c3 = new Cylinder(7.0,8.0);
		
		assertEquals(1.0,c1.getHeight(),0.01);
		assertEquals(10,c2.getHeight(),0.01);
		assertEquals(8.0,c3.getHeight(),0.01);
		
	}
	
	@Test
	public void testGetVolume() {
		Cylinder c = new Cylinder(7.0,8.0);
		assertEquals(1231.504,c.getVolume(),0.01);
	}
}
