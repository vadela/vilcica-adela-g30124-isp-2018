package packageg30124.vilcica.adela.lab4.ex7;

import packageg30124.vilcica.adela.lab4.ex3.Circle;

public class Cylinder extends Circle{
	
	private double height;

	public Cylinder() {
		height=1.0;
	}
	
	public Cylinder(double radius) {
		super(radius);	
		this.height=10;
	}
	
	public Cylinder(double radius, double height) {
		super(radius);
		this.height=height;	
	}
	
	public double getHeight() {
		return height;
	}
	

	@Override
	public double getArea() {
		super.getArea();
		return Math.PI*super.getRadius()*super.getRadius();
	}

	public double getVolume() {
		return getArea()*getHeight();
	}
}
